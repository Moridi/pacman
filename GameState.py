class Actors:
    actors = ['P', 'Q']

class Foods:
    foods = ['1', '2', '3']

    eatableFoods = {
        'P':['1', '3'],
        'Q':['2', '3']
    }

class GameState:

    walls = None
    hashToState = {}

    def __init__(self, M, N):
        self.foods = {}
        self.agents = {}
        self.hash = None
        self.actor = None
        self.cost = 0

    @staticmethod
    def setupWalls(M, N):
        GameState.walls = [[False for x in range(N)] for y in range(M)]

    @staticmethod
    def getWalls(self):
        return GameState.walls
    
    @staticmethod
    def setWall(x, y):
        GameState.walls[x][y] = True

    @staticmethod
    def hasWall(x, y):
        return GameState.walls[x][y]
    
    def increaseCost(self):
        self.cost += 1

    def getCost(self):
        return self.cost

    def hasAgent(self, x, y):
        for agent in self.agents.keys():        
            if (self.agents[agent] == (x, y)):
                return True
        return False
    
    def setFood(self, x, y, foodType):
        if (foodType != None):
            self.foods[(x, y)] = foodType
    
    def setAgent(self, x, y, agentType):
        self.agents[agentType] = (x, y)

    def setActor(self, agentType):
        self.actor = agentType

    def getActor(self):
        return self.actor

    def changeActor(self):
        actors = list(self.agents.keys())
        i = actors.index(self.actor) + 1
        self.actor = actors[i % len(actors)]

    def hasFood(self, x, y):
        return (x, y) in self.foods

    def getHashable(self, field):
        newList = []
        
        for instance in field:
            newList.append(str(instance))

        hashable = str(newList)
        return hashable

    def getHashables(self):
        return (self.getHashable(self.foods.items()),
                self.getHashable(self.agents.items()))

    def setHash(self):
        hashableFields = self.getHashables()
        self.hash = hash(hashableFields)
        GameState.hashToState[self.hash] = self

    def getHash(self):
        return self.hash

    def getPosition(self):
        return self.agents[self.actor]

    def setPosition(self, position):
        self.agents[self.actor] = position

    def eatFood(self, x, y):
        del self.foods[(x, y)]

    def isEatableFood(self, x, y):
        return (self.foods[(x, y)] in
                Foods.eatableFoods[self.actor])

    def isGoalState(self):
        return len(self.foods) == 0

    def __gt__(self, other):
        return self.cost > other.cost

    def __lt__(self, other):
        return self.cost < other.cost

    def getFoodsAsList(self):
        foods = []
        for position, foodType in self.foods.items():
            if (foodType in Foods.eatableFoods[self.actor]):
                foods.insert(0, position)
        return foods