from MapLoader import MapLoader
from IterativeDeepeningSearch import IterativeDeepeningSearch
from BreadthFirstSearch import BreadthFirstSearch
from AStarSearch import AStarSearch

if __name__ == "__main__":
    mapLoader = MapLoader('test1')
    mapLoader.loadMap()
    initialState = mapLoader.setupInitialState()
    searchProblem = AStarSearch(initialState)
    searchProblem.search()