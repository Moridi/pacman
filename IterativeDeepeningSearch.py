from DataStructures import Queue
from DataStructures import Stack
from SearchProblem import SearchProblem
from GameState import Actors
from Actions import Actions
import copy

class IterativeDeepeningSearch(SearchProblem):
    def __init__(self, initialState):
        super().__init__(initialState)

    def setupStack(self, startState):
        self.stack = Stack()
        self.stack.push(startState)
        self.secondaryQueue = Queue()
        self.visited = {}
        self.visited[startState.getHash()] = True

    def isDfsDone(self):
        return self.stack.isEmpty() or self.isGoalStateVisited

    def dls(self, maxDepth):
        depth = maxDepth

        while (not self.isDfsDone()):
            firstNode = self.stack.pop()
            self.finalCost = firstNode.getCost()
            depth = maxDepth - self.finalCost

            if (firstNode.isGoalState()):
                self.isGoalStateVisited = True
            elif (depth > 0):
                self.expandNode(firstNode, self.stack)
            else:
                self.secondaryQueue.push(firstNode)

    def copyMaxDepthElements(self):
        self.stack.list = list(self.secondaryQueue.list)
        self.secondaryQueue = Queue()

    def search(self):
        startState = self.setStartState()
        self.setupStack(startState)

        maxDepth = 0
        while(not self.isGoalStateVisited):
            maxDepth += 1
            self.dls(maxDepth)
            self.copyMaxDepthElements()

        print(f"Final cost = {self.finalCost}\n" + 
                f"Number of Independent States: {self.independentStates}\n" +
                f"Number of Visited States: {self.numberOfStates}")
