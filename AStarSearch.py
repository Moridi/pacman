from SearchProblem import SearchProblem
from DataStructures import MinGetter
from DataStructures import Queue
from Actions import Actions
from GameState import Actors
import copy

class AStarSearch(SearchProblem):
    def __init__(self, initialState):
        super().__init__(initialState)

    def modifyOpenList(self, newState, dataStructure):
        currentCost = newState.getCost()
        estimatedCost = self.manhattanDistance(
                newState.getPosition(), newState.getFoodsAsList())

        estimatedCost += currentCost
        
        dataStructure.push(newState, estimatedCost)

    def createNewState(self, newState, dataStructure):
        stateHash = newState.getHash()
        if (stateHash not in self.visited):
            self.modifyOpenList(newState, dataStructure)
        return newState
    
    def bfsUtil(self):
        firstNode = self.queue.pop()

        self.visited[firstNode.getHash()] = True

        if (firstNode.isGoalState()):
            self.finalCost = firstNode.getCost()
            self.isGoalStateVisited = True
        else:
            self.expandNode(firstNode, self.queue)

    def isBfsDone(self):
        return self.queue.isEmpty() or self.isGoalStateVisited

    def setupQueue(self, startState):
        self.queue = MinGetter()
        self.queue.push(startState, 0)
        self.visited[startState.getHash()] = True

    def search(self):
        startState = self.setStartState()
        self.setupQueue(startState)

        while(not self.isBfsDone()):
            self.bfsUtil()
        
        print(f"Final cost = {self.finalCost}")

    def manhattanDistance(self, position, goals):
        x1, y1 = position

        distance = 0
        for goal in goals:
            x2, y2 = goal
            distance += abs(x1 - x2) + abs(y1 - y2)

        return distance