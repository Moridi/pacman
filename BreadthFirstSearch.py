from DataStructures import Queue
from SearchProblem import SearchProblem
from GameState import Actors
from Actions import Actions
import copy

class BreadthFirstSearch(SearchProblem):
    def __init__(self, initialState):
        super().__init__(initialState)

    def bfsUtil(self):
        firstNode = self.queue.pop()

        if (firstNode.isGoalState()):
            self.finalCost = firstNode.getCost()
            self.isGoalStateVisited = True
        else:
            self.expandNode(firstNode, self.queue)

    def isBfsDone(self):
        return not self.queue.isEmpty() and not self.isGoalStateVisited

    def setupQueue(self, startState):
        self.queue = Queue()
        self.queue.push(startState)
        self.visited[startState.getHash()] = True

    def search(self):
        startState = self.setStartState()
        self.setupQueue(startState)
        while(self.isBfsDone()):
            self.bfsUtil()
        
        print(f"Final cost = {self.finalCost}\n" + 
                f"Number of Independent States: {self.independentStates}\n" +
                f"Number of Visited States: {self.numberOfStates}")
