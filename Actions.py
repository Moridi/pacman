from GameState import GameState
import copy

class Actions:
    directions = [(0, 1), (0, -1), (1, 0), (-1, 0)]

    @staticmethod
    def checkFoodType(state, actions, row, column):
        if (state.isEatableFood(row, column)):
            actions.append([(row, column), True])
    
    @staticmethod
    def checkPositionValidity(state, actions, row, column):
        if (not state.hasAgent(row, column)):
            if(state.hasFood(row, column)):
                Actions.checkFoodType(state, actions, row, column)
            else:
                actions.append([(row, column), False])

    @staticmethod
    def getPossibleActions(state, position):
        actions = []
        for direction in Actions.directions:
            row = direction[0] + position[0]
            column = direction[1] + position[1]
            if (not GameState.hasWall(row, column)):
                Actions.checkPositionValidity(state, actions, row, column)
        
        return actions