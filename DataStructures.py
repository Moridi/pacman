from GameState import GameState

class DataStructure:
    def __init__(self):
        self.list = []
    
    def pop(self):
        return self.list.pop()

    def printList(self):
        for x in self.list:
            print(f"{x.getActor()}:" +
                f"({x.getPosition()[0]}, " +
                f"{x.getPosition()[1]})", end=", ")
        print()

    def isEmpty(self):
        return len(self.list) == 0

class Stack(DataStructure):
    def __init__(self):
        super().__init__()

    def push(self,item):
        self.list.append(item)

class Queue(DataStructure):
    def __init__(self):
        super().__init__()

    def push(self,item):
        self.list.insert(0, item)
    
    def has(self, item):
        return item in self.list

class MinGetter(DataStructure):
    def __init__(self):
        super().__init__()
        self.openList = {}

    def modifyQueue(self, newItem, newPriority):
        insertedPriority = self.openList[newItem.getHash()]

        if (newPriority < insertedPriority
                and (insertedPriority, newItem) in self.list):
            idx = self.list.index((insertedPriority, newItem))
            self.list[idx] = (newPriority, newItem)
            self.openList[newItem.getHash()] = newPriority

    def push(self, newItem, newPriority):
        if (not newItem.getHash() in self.openList):
            self.list.insert(0, (newPriority, newItem))
            self.openList[newItem.getHash()] = newPriority    

    def pop(self):
        element = min(self.list)
        del self.list[self.list.index(element)]
        del self.openList[element[1].getHash()]
        return element[1]