from Actions import Actions
from DataStructures import Stack
from GameState import Actors
import copy

class SearchProblem:
    def __init__(self, initialState):
        self.initialState = initialState
        self.visited = {}
        self.isGoalStateVisited = False
        self.finalCost = 0

    def setStartState(self):
        actor = Actors.actors[0]
        startState = copy.deepcopy(self.initialState)
        startState.setActor(actor)
        startState.setHash()
        return startState

    def getNewState(self, initiator, action):
        position = action[0]
        isThereEatableFood = action[1]

        newState = copy.deepcopy(initiator)
        newState.setPosition(position)
        newState.increaseCost()

        if (isThereEatableFood):
            newState.eatFood(position[0], position[1])

        newState.setHash()

        return newState
    
    def changeActor(self, initiator):
        newState = copy.deepcopy(initiator)
        newState.changeActor()

        return newState
    
    def createNewState(self, newState, dataStructure):
        stateHash = newState.getHash()

        if (stateHash not in self.visited):
            dataStructure.push(newState)
            self.visited[newState.getHash()] = True

    def iterateNewNodes(self, initiator, actions, dataStructure):
        for action in actions:
            newState = self.getNewState(initiator, action)
            self.createNewState(newState, dataStructure)

    def iteratePossibleStates(self, initiator, dataStructure):
        position = initiator.getPosition()
        actions = Actions.getPossibleActions(initiator, position)
        self.iterateNewNodes(initiator, actions, dataStructure)

    def expandNode(self, node, dataStructure):
        changedActor = self.changeActor(node)
        self.iteratePossibleStates(changedActor, dataStructure)
        self.iteratePossibleStates(node, dataStructure)

    def manhattanDistance(self, position, goals):
        x1, y1 = position

        maxDistance = -1
        for goal in goals:
            x2, y2 = goal
            distance = abs(x1 - x2) + abs(y1 - y2)
            maxDistance = max(distance, maxDistance)

        return maxDistance