from GameState import GameState, Foods, Actors

MAP_DIRECTORY = "Testcases/"
WALL_SYMBOL = "%"

class MapLoader:
    def __init__(self, file_name):
        self.path = MAP_DIRECTORY + file_name
        self.lines = []
    
    def loadMap(self):
        with open(self.path) as file_descriptor:
            line = file_descriptor.readline()
            while line:
                self.lines.append(line)
                line = file_descriptor.readline()
    
    def getMap(self):
        return self.lines

    def getMapSize(self):
        return len(self.lines), len(self.lines[0]) - 1
    
    def setupLineWalls(self, row, line):
        for column, node_symbol in enumerate(line):
            if (node_symbol == WALL_SYMBOL):
                GameState.setWall(row, column)

    def setupWalls(self):
        for row, line in enumerate(self.lines):
            self.setupLineWalls(row, line)
    
    def setupLineFoods(self, row, line, initialState):
        for column, node_symbol in enumerate(line):
            if (node_symbol in Foods.foods):
                initialState.setFood(row, column, node_symbol)

    def setupFoods(self, initialState):
        for row, line in enumerate(self.lines):
            self.setupLineFoods(row, line, initialState)
    
    def setupLineAgents(self, row, line, initialState):
        for column, node_symbol in enumerate(line):
            if (node_symbol in Actors.actors):
                initialState.setAgent(row, column, node_symbol)

    def setupAgents(self, initialState):
        for row, line in enumerate(self.lines):
            self.setupLineAgents(row, line, initialState)
    
    def setupInitialState(self):
        M, N = self.getMapSize()
        GameState.setupWalls(M, N)
        initialState = GameState(M, N)
        self.setupWalls()
        self.setupFoods(initialState)
        self.setupAgents(initialState)
        initialState.setHash()
        return initialState
