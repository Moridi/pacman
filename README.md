# Pacman Game Simulator

Goal of this computer assignment is getting more familier with searching algorithms. From searching algorithms we will apply `BFS`, `IDS`, and `Astar Search`. All of these algorithms have an optimal answer but with different exectuion time.

## Uninformed Search

### Creating Uninformed Pacman World
In this kind of solution, our world is a little difference from first definition of `PacmanWorld`. We should get all feasible actions which every agent can make; therefore, we define a new world for this kind of solution which inherited our first world.

### BFS

Initially, for running `BFS` algorithm we make our test world. Then our algorithm would start to run. 

This algorithm has a queue as its frontier and a set as its explored. 
* At first we add intial state into frontier and then pop it from the queue. 
* Then all possible actions from initial values which does not explored before, will add to the frontier 
* and next we will redo these steps until we find goal state.

`BFS` is an optimal search algotrithm.

### IDS

This algorithm has a stack as its frontierand a set as its explored. 
* We have a loop to increase depth in every iteration of it. Actualy at every iteration we increase depth and run inner loop which is a limited DFS. 
* At every iteration we reset all of data structures to their initial values.
* In limited DFS : 
    * We pop first node from stack
    * If it's depth is our limit we didn't add it's children to frontier
    * Else we add chidlren to frontier



## Informed Search

For our Informed world we create a new class which have some extra method from uninformed world. This class has some methods to compute heuristic of the world.



### What is our Heuristic?
Our heueristic is `minimum manhatan distance from all foods + [number of remained food after eating nearest food / 2]` .

### Why this Heuristic is Admissable?
This heuristic is admissable. Assume that our minimum manhatan distance to all food be `min(x)` and `x` be the nearest food. Also assume that `n` be number of all remaning foods after eating x. 

From definition a heuristic is addmisable that $h(n) \leq g(n) $ which $h(n)$ is heuristic of state and $g(n)$ is actual path cost to goal.  

**PROVE:** Mininmum path from our state to goal is `min(x) + n` which is eating x by exploring min(x) in one path and eating n other foods with just one move for every of them; beside, our heuristic is `min(x) + n/2` which always is less than equal to minimum path. 

### Why this Heuristic is Consistent?
This heuristic is also consistent. Assume above symbols. 

From definition a heuristic is consistent if we have a state `C` after state `A` real cost of A to C be greater than equal of cost that implied by heuristics. 
$cost(AtoC) \geq h(A) - h(C)$

**PROVE:** Worst case happend when in state `C` and `A`, agent goes manhatan distance to got `C`. We can assume `min(x1)` as manhatan distance of first move(state A), and `min(x2)` as manhatan distance of second move(state C).

We should prove that :
$$
cost(AtoC) \geq h(A) - h(C)
$$

We have : 
$$
h(A) = min(x_1) + \lfloor \frac{n}{2} \rfloor
$$

$$
h(C) = min(x_2) + \lfloor \frac{n-1}{2} \rfloor
$$

$$
cost(AtoC) = min(x_1)
$$

So for this eqution we have : 
$$
h(A) - h(C) = min(x_1) + \lfloor \frac{n}{2} \rfloor - min(x_2) - \lfloor \frac{n-1}{2} \rfloor 
\begin{equation*}
 = \begin{cases}
             min(x_1) - min(x_2)   & \text{if } n = 0 \text{ or } n \text{ is odd } \\
             min(x_1) - min(x_2) + 1  & \text{if } n \neq 0 \text{ and } n \text{ is even }
       \end{cases} \quad
\end{equation*}
$$


$$
cost(AtoC) = min(x_1)
$$

From this we can recognize from above equetions that bellow assignment should  always be less than equal of `zero` which it always is.
$$
\begin{equation*}
 0 \geq \begin{cases}
             - min(x_2)   & \text{if } n = 0 \text{ or } n \text{ is odd } \\
             - min(x_2) + 1  & \text{if } n \neq 0 \text{ and } n \text{ is even }
       \end{cases} \quad
\end{equation*}
$$

### Astar Search
This algorithm is very similar to `BFS` algorithm, but it uses a priority queue as it's frontier instead. At first we make our test world (here I used `ui_world` which ran test1 as initial world). 
Then our algorithm started to run. 

This algorithm has a priority queue as it's frontier and a set as it's explored. It also use f(state) which is h(s) + cost_to(s) as priority of every node in queue.
* At first we add intial state into frontier and then pop it from the queue. 
* Then all possible actions from initial values which does not explored before, will add to the frontier 
* and next we will redo these steps until we find goal state.


### Comparing Algorithms 

#### Optimality
All of these algorithms return optimal answers. BFS is optimal because we move in depth. IDS is optimal too because we limit depth of DFS. In A* if we have an admissable and consistent heuristic, we can say it is an optimal algorithm. Here we have a heueristic with these charactrastics which proven above.  

#### Time Complexity
For `BFS` if we assume that we have b-ary tree of depth d time compelxity is number of nodes in that which is $O(b^d)$.

For `IDS` Because of we have limited depth first search we should have time complexity of every itteration. So time complexity is $O((d+1)b^0 + d b^1 + (d-1)b^2 + … + b^d)$.

For `A*Search` time complexity is number of nodes which heuristic expands. 

As you see in above tables time order is  $IDS \gt A^* \gt BFS$. Altough number of seen states is in $IDS \gt BFS \gt A^*$ but time of A* is a little more than bfs. it is because of **computing heuristic** overheads. For bigger maps which this overhead can be ignored we can say time of A* is less than BFS; on the other hand, IDS have a lot of more time complexity in this problem. Becasue we have a big depth for solotiouns. It is always slower than BFS and A*.

#### Space Complexity
Space complexity of `BFS` is like it's time complexity which is number of it nodes. ( $O(b^d)$ ).

`IDS` have less space for computing. It have an space complexity of $O(bd)$ and its why people use IDS instead of BFS.

`A*` also like BFS have an exponential space comlexity but because of less nodes which seen in this algorithm we can say we hope it got less space. 

To sum up we can say here A* and BFS can be good answers for this problem. They have near time complexity and have less seen state. On the other hand I work on optimization of BFS more. At my first try BFS had more execution time and I started to change my data structures into string instead of 2d array. This move save a lot of time for me in BFS. If I had that matrix at first it may got more time to execute from A*. 
